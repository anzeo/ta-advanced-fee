angular.module("app.menu").controller("MenuDetailsController", function (CartService : ta.fee.ICartService, data) {
    "use strict";
    var controller = this;
    controller.addSelectedItemToCart = addSelectedItemToCart;
    controller.selectedItem = data;

    function addSelectedItemToCart(){
        CartService.addItem(controller.selectedItem);
    }
});