angular.module("app.menu").service("MenuService", function($http : angular.IHttpService){

    var service : ta.fee.IMenuService = this;
    service.loadUri = loadUri;

    var remoteBase : string = "http://localhost:1337/items/";

    return service;

    function loadUri(stateParams : angular.ui.IStateParamsService) : angular.IPromise<any>{
        return $http.get(mapLocalToRemoteUri(stateParams)).then(function(response){
            return response.data;
        });
    }

    function mapLocalToRemoteUri($stateParams) : string {
        var remoteUri = remoteBase;
        if($stateParams.submenu){
            remoteUri += $stateParams.submenu + "/";
        }
        if($stateParams.item){
            remoteUri += $stateParams.item + "/";
        }
        if($stateParams.selectedItem){
            remoteUri += $stateParams.selectedItem;
        }
        return remoteUri;
    }
});