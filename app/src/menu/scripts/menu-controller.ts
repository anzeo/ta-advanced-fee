angular.module("app.menu").controller("MenuController", function ($state : angular.ui.IStateService, MenuService : ta.fee.IMenuService, data) {
    "use strict";
    var controller = this;
    controller.goBack = goBack;
    controller.goToItem = goToItem;
    controller.data = data;

    function goBack() {
        goTo(controller.data._links.parent.href);
    }

    function goToItem(item) {
        if(item._links.children){
            goTo(item._links.children.href);
        } else {
            goTo( item._links.self.href, true);
        }
    }

    function goTo(url : string, goToDetails? : boolean) {
        var parts = url.split("items/")[1].split("/");
        var params = {
            submenu: parts[0],
            item: parts[1],
            selectedItem: parts[2]
        };
        if(!goToDetails){
            $state.go('menu', params);
        } else {
            $state.go('menu.details', params);
        }
    }
});