angular.module("app.menu", ["ui.router"]).config(function ($stateProvider : angular.ui.IStateProvider) {
    $stateProvider.state('menu', {
        url: '/menu/:submenu/:item',
        params: {
            submenu: {
                value: null,
                squash: true
            },
            item: {
                value: null,
                squash: true
            }
        },
        templateUrl: 'src/menu/views/menu.html',
        controller: 'MenuController',
        controllerAs: 'MenuController',
        resolve: {
            data: function($stateParams : angular.ui.IStateParamsService, MenuService : ta.fee.IMenuService){
                return MenuService.loadUri($stateParams);
            }
        }
    }).state('menu.details', {
        url: '/:selectedItem',
        templateUrl: 'src/menu/views/menu-details.html',
        controller: 'MenuDetailsController',
        controllerAs: 'MenuDetailsController',
        resolve: {
            data: function($stateParams : angular.ui.IStateParamsService, MenuService : ta.fee.IMenuService){
                return MenuService.loadUri($stateParams);
            }
        }
    });
});