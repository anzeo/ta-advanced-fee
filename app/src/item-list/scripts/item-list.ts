angular.module("app.itemList").directive("itemList", function(){
   return {
        scope: {
            items:"=itemList"
        },
        templateUrl: "src/item-list/views/item-list.html",
        link: function(scope : ta.fee.IItemListScope){
            scope.$watch('items.length', function(){
                var itemMap : {[ key: string] : number } = {};
                for(var i = 0; i < scope.items.length; i++){
                    var item = scope.items[i];
                    itemMap[item.title] = itemMap[item.title] || 0;
                    itemMap[item.title]++;
                }
                scope.mappedItems = itemMap;
            });
        }
   };
});