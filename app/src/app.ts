angular.module('app', ["ui.router", "app.itemList", "app.menu", "app.orders", "app.cart"])
    .config(function ($urlRouterProvider : angular.ui.IUrlRouterProvider) {
        $urlRouterProvider.otherwise('/menu');
    }
);