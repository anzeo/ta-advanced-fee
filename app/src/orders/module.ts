angular.module("app.orders", ["ui.router"]).config(function ($stateProvider : angular.ui.IStateProvider) {
    $stateProvider.state('orders', {
        url: '/orders',
        templateUrl: 'src/orders/views/orders.html'
    });
});