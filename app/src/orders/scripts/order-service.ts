angular.module("app.orders").service("OrderService", function ($http : angular.IHttpService) : ta.fee.IOrderService {
    "use strict";

    return {
        loadOrders: loadOrders,
        placeOrder: placeOrder
    };

    function errorHandler(data, status) : void {
        throw "Error("+ status +") trying to complete the request"
    }

    function loadOrders() : angular.IPromise<any> {
        return $http({method: 'GET', url: 'http://localhost:1337/orders'})
            .error(errorHandler);
    }

    function placeOrder(items : Array<ta.fee.IOrderItem>) : angular.IPromise<any> {
        var itemIds = [];
        for (var i = 0; i < items.length; i++) {
            itemIds.push(items[i].id);
        }
        return $http({
            url: 'http://localhost:1337/orders',
            method: "POST",
            data: {
                items: itemIds
            },
            headers: {'Content-Type': 'application/json'}
        }).error(errorHandler);
    }
});