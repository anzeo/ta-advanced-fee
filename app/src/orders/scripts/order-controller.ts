angular.module("app.orders").controller("OrderController", function (OrderService : ta.fee.IOrderService) {
    "use strict";
    var controller = this;
    controller.orders = [];

    OrderService.loadOrders().then(function (result) {
        controller.orders = result.data._embedded.items;
    });
});