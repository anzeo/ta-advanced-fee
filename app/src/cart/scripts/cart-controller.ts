angular.module("app.cart").controller("CartController", function (CartService : ta.fee.ICartService) {
    "use strict";
    var controller = this;
    controller.submitCart = submitCart;
    controller.removeItem = removeItem;
    controller.itemsInCart = CartService.items;

    function submitCart() : void {
        CartService.submit().then(function(){
            alert("Your order has been successfully processed");
        });
    }

    function removeItem($event : angular.IAngularEvent, index : number) : void{
        $event.stopPropagation();
        controller.itemsInCart.splice(index,1);
    }
});