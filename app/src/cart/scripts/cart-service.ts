angular.module("app.cart").service("CartService", function (OrderService : ta.fee.IOrderService) : ta.fee.ICartService {
    "use strict";
    var service : ta.fee.ICartService = this;

    service.submit = submit;
    service.addItem = addItem;

    service.items = [];

    return service;

    function addItem(item) {
        service.items.push(item);
    }

    function submit() : angular.IPromise<any> {
        return OrderService.placeOrder(service.items).then(function () {
            service.items.length = 0;
        });
    }
});