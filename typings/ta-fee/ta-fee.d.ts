declare module ta.fee {
    interface IMenuService {
        loadUri($stateParams : angular.ui.IStateParamsService): angular.IPromise<any>
    }

    interface IOrderService {
        loadOrders(): angular.IPromise<any>
        placeOrder(items : Array<IOrderItem>): angular.IPromise<any>
    }

    interface IOrderItem {
        id: number
    }

    interface ICartService {
        submit(): angular.IPromise<any>
        addItem(item: IOrderItem): void
        items: Array<IOrderItem>
    }

    interface IItemListScope extends angular.IScope{
        items: Array<any>
        mappedItems: {[key: string] : number }
    }
}