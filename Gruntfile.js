module.exports = function (grunt) {
    "use strict";

    // Configure tasks
    grunt.initConfig({
        less: {
            dist: {
                files: {
                    "app/styles/app.css": ["app/styles/app.less"]
                }
            }
        },
        karma: {
            unit: {
                configFile: "config/karma.conf",
                singleRun: true
            }
        },
        typescript: {
            base: {
                src: ['app/src/**/*.ts', 'specs/**/*.ts'],
                dest: './',
                options: {
                    module: 'amd',
                    target: 'es5',
                    references: [
                        "typings/tsd.d.ts"
                    ]
                }
            }
        }
    });
    
    // Load task plugins
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-typescript');

    // Define tasks
    grunt.registerTask('default', ['typescript', 'karma', 'less']);
};