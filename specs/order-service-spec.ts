describe("The order service", function(){
    var OrderService : ta.fee.IOrderService;
    var $httpBackend : angular.IHttpBackendService;

    beforeEach(module("app.orders"));
    beforeEach(inject(function(_OrderService_ : ta.fee.IOrderService, _$httpBackend_ : angular.IHttpBackendService){
        OrderService = _OrderService_;
        $httpBackend = _$httpBackend_;

        // mock out calls
        $httpBackend.when('GET', 'http://localhost:1337/orders').respond({});
        $httpBackend.when('POST', 'http://localhost:1337/orders').respond({});
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it("should load existing orders", function(){
        $httpBackend.expectGET('http://localhost:1337/orders');
        OrderService.loadOrders();
        $httpBackend.flush();
    });

    it("should post a valid order to the backend", function(){
        $httpBackend.expectPOST('http://localhost:1337/orders');
        OrderService.placeOrder([]);
        $httpBackend.flush();
    });
});